#!/bin/bash

#Dev Terraform Initialization
rm -rf ./TCAWSKubeDeploy
git clone https://gitlab.com/gowrishankarsukumar/TCAWSKubeDeploy.git --branch ${AWS_KUBE_DEPLOY_VERSION}
cd ./TCAWSKubeDeploy
terraform init --backend-config="access_key=$AWS_ACCESS_KEY_ID" --backend-config="secret_key=$AWS_SECRET_ACCESS_KEY" --backend-config="dynamodb_table=$DYNAMODB_TABLE" --backend-config="bucket=$BUCKET"
